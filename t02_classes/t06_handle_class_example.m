%% clear
clear all global
restoredefaultpath

%% get a normal class
normal_class1 = handle_class_example.MyNormalClass();
normal_class1.number = 1;

%% copy it by assignment
copy_of_normal_class1 = normal_class1;
copy_of_normal_class1.number = 100;

%% see what it is both classes
normal_class1
copy_of_normal_class1

%% get a handle class
handle_class1 = handle_class_example.MyHandleClass();
handle_class1.number = 1;

%% copy it by assignment
copy_of_handle_class1 = handle_class1;
copy_of_handle_class1.number = 100;

%% see what it is both classes
handle_class1
copy_of_handle_class1

%% get a handle class
handle_class1 = handle_class_example.MyHandleClass();
handle_class1.number = 1;

%% copy it by copy
copy_of_handle_class1 = copy(handle_class1);
copy_of_handle_class1.number = 100;

%% see what it is both classes
handle_class1
copy_of_handle_class1